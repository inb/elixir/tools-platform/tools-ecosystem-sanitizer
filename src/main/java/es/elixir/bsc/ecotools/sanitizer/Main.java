/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.elixir.bsc.ecotools.sanitizer;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import es.elixir.bsc.ecotools.sanitizer.errors.ToolsEcosystemValidationMessage;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

public class Main {

    private final static String HELP = 
            "tools-sanitizer -g -u -p [-b]\n\n" +
            "parameters:\n\n" +
            "-h (--help)     - this help message\n" +
            "-g (--git)      - git endpoint\n" +
            "-u (--user)     - github username\n" +
            "-p (--password) - github pasword\n" +
            "-b (--branch)   - git remote branch ['origin/master']\n\n" +
            "example: >java -jar tools-sanitizer.jar -g https://myrepo.git -u redmitry -p xyz\n";

    public final static String BRANCH = "origin/master";

    public static void main(String[] args) throws IOException {
        
        Map<String, List<String>> params = parameters(args);
        
        if (params.get("-h") != null ||
            params.get("--help") != null) {
            System.out.println(HELP);
            System.exit(0);
        }
        
        List<String> git = params.get("-g");
        if (git == null) {
            git = params.get("--git");
        }

        List<String> user = params.get("-u");
        if (user == null) {
            user = params.get("--user");
        }

        List<String> password = params.get("-p");
        if (password == null) {
            password = params.get("--password");
        }

        List<String> branch = params.get("-b");
        if (branch == null) {
            branch = params.get("--branch");
        }
        final String b = branch == null || branch.isEmpty() ? null : branch.get(0);

        final String g = git == null || git.isEmpty() ? null : git.get(0);
        final String u = user == null || user.isEmpty() ? null : user.get(0);
        final String p = password == null || password.isEmpty() ? null : password.get(0);

        if (g == null) {
            System.out.println("no git url provided!");
            System.out.println(HELP);
            System.exit(1);
        }
        
        process(g, b != null ? b : BRANCH, u, p);
    }
    
    private static void process(final String endpoint, final String branch, 
            final String user, final String password) {

        final int idx = branch.lastIndexOf('/');
        final String local = idx < 0 ? branch : branch.substring(idx + 1);

        // ignore file names validation as we do not use 'real' file system
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        Configuration config = Configuration.unix().toBuilder()
                                    .setWorkingDirectory("/")
                                    .build();

        FileSystem fs = Jimfs.newFileSystem(config);
        final Path root = fs.getPath("/");
        
        try (Git git = Git.cloneRepository()
                         .setURI(endpoint)
                         .setDirectory(root)
                         .setBranch(local)
                         .setRemote(branch)
                         .call()) {
            
            validate(root);
            
//            git.add().addFilepattern(".").call();
//            git.commit().setMessage("openebench metrics " + Instant.now()).call();
//            git.push().setRemote(branch).setDryRun(true).setCredentialsProvider(
//                    new UsernamePasswordCredentialsProvider(user, password)).call();
        } 
        catch (GitAPIException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
    }

    private static List<ToolsEcosystemValidationMessage> validate(final Path root) {
        final List<ToolsEcosystemValidationMessage> messages = new ArrayList();
        final ServiceLoader<ValidateService> loader = ServiceLoader.load(ValidateService.class);
        final Iterator<ValidateService> iterator = loader.iterator();
        while(iterator.hasNext()) {
            final ValidateService validator = iterator.next();
            validator.validate(root, messages);
        }
        return messages;
    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-g":
                case "--git":
                case "-b":
                case "--branch":
                case "-u":
                case "--user":
                case "-p":
                case "--password":
                case "-h":
                case "--help": values = parameters.get(arg);
                               if (values == null) {
                                   values = new ArrayList(); 
                                   parameters.put(arg, values);
                               }
                               break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }
}

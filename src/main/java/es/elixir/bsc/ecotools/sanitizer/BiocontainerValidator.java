/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.elixir.bsc.ecotools.sanitizer;

import es.elixir.bsc.ecotools.sanitizer.errors.ToolsEcosystemValidationMessage;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.yaml.snakeyaml.Yaml;

/**
 * @author Dmitry Repchevsky
 */

public class BiocontainerValidator implements ValidateService {
    
    @Override
    public void validate(final Path root, final List<ToolsEcosystemValidationMessage> messages) {
        
        final Yaml yaml = new Yaml();
        HashMap<String, Path> files = new HashMap();

        // index .json files by biotoolsID
        try (Stream<Path> stream = Files.walk(root)){
            final Iterator<Path> iter = stream.iterator();
            while (iter.hasNext()) {
                final Path path = iter.next();
                if (Files.isRegularFile(path) && 
                    path.toString().startsWith(ToolProvider.BIOCONTAINERS.PREFIX) &&
                    path.toString().endsWith(ToolProvider.BIOCONTAINERS.SUFFIX)) {
        
                    try (Reader reader = Files.newBufferedReader(path)) {
                        final Map<String, Object> attributes = yaml.load(reader);
                        validate(path, attributes, messages);
                    } catch (Exception ex) {
                        Logger.getLogger(BiocontainerValidator.class.getName()).log(Level.SEVERE, ex.getMessage());
                    }
                }
            }
        } catch(IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void validate(final Path path, final Map<String, Object> attributes, 
            final List<ToolsEcosystemValidationMessage> messages) {

        final Object name = attributes.get("name");
        
        String biotoolsID = getIdentifier(attributes, "biotools:");
        String doi = getIdentifier(attributes, "doi:");
        
        System.out.println("=> name: " + name + " biotools: " + biotoolsID + " doi: " + doi);
    }
    
    private String getIdentifier(final Map<String, Object> attributes, final String prefix) {
        String id = null;
        
        final Object identifiers = attributes.get("identifiers");
        if (identifiers instanceof String) {
            if (identifiers.toString().startsWith(prefix)) {
                id = identifiers.toString().substring(prefix.length());
            }
        } else if (identifiers instanceof List) {
            final List list = (List)identifiers;
            for (Object o : list) {
                final String altID = o.toString();
                if (altID.startsWith(prefix)) {
                    id = altID.substring(prefix.length());
                }
            }
        }
        return id;
    
    }
}

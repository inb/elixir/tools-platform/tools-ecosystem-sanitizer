/**
 * *****************************************************************************
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.elixir.bsc.ecotools.sanitizer;

import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.storage.file.FileBasedConfig;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

public class JgitSytemReaderHack extends SystemReader {

    private final SystemReader sr;

    public JgitSytemReaderHack(SystemReader sr) {
        this.sr = sr;
    }

    @Override
    public void checkPath(String path) throws CorruptObjectException {
    }

    @Override
    public void checkPath(byte[] path) throws CorruptObjectException {
    }


    @Override
    public String getHostname() {
        return sr.getHostname();
    }

    @Override
    public String getenv(String arg0) {
        return sr.getenv(arg0);
    }

    @Override
    public String getProperty(String arg0) {
        return sr.getProperty(arg0);
    }

    @Override
    public FileBasedConfig openUserConfig(Config arg0, FS arg1) {
        return sr.openUserConfig(arg0, arg1);
    }

    @Override
    public FileBasedConfig openSystemConfig(Config arg0, FS arg1) {
        return sr.openSystemConfig(arg0, arg1);
    }

    @Override
    public long getCurrentTime() {
        return sr.getCurrentTime();
    }

    @Override
    public int getTimezone(long arg0) {
        return sr.getTimezone(arg0);
    }
}
